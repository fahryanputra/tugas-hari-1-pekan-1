<?php

class Harimau extends Hewan {
  public $jumlahKaki = 4;
  public $keahlian = 'lari cepat';
  public $attackPower = 7;
  public $defencePower = 8;

  function __construct($nama) {
    $this->nama = $nama;
  }

  public function getInfoHewan() {
    echo "nama : {$this->nama} <br>";
    echo "jumlah kaki: {$this->jumlahKaki} <br>";
    echo "keahlian: {$this->keahlian} <br>";
    echo "attack power: {$this->attackPower} <br>";
    echo "defence power: {$this->defencePower}";
  }

  public function atraksi() {
    echo "{$this->nama} sedang {$this->keahlian}.";
  }

  public function serang($diserang) {
    echo "{$this->nama} sedang menyerang {$diserang->nama}.";
  }

  public function diserang($penyerang) {
    echo "{$this->nama} sedang diserang.";
    return $this->darah = $this->darah - ($penyerang->attackPower / $this->defencePower);
  }
}


 ?>
