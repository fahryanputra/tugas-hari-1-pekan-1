<?php

spl_autoload_register(function ($class_name) {
  include $class_name . '.php';
});

$elang_1 = new Elang("elang_1",);
$harimau_1 = new Harimau("harimau_1");

$elang_1->getInfoHewan();
echo "<br><br>";
$harimau_1->getInfoHewan();

echo "<br><br>";

$elang_1->atraksi();
echo "<br>";
$harimau_1->atraksi();

echo "<br><br>";

$elang_1->serang($harimau_1);
echo "<br>";
$elang_1->diserang($harimau_1);

echo "<br><br>";

echo "darah elang: {$elang_1->darah}.";
?>
