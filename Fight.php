<?php

abstract class Fight {
  public $attackPower;
  public $defencePower;

  public function __construct($attackPower, $defencePower) {
    $this->attackPower = $attackPower;
    $this->defencePower = $defencePower;
  }

  abstract public function serang($diserang);
  abstract public function diserang($penyerang);
}

 ?>
