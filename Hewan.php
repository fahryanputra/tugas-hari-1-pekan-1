<?php

abstract class Hewan extends Fight {
  public $nama;
  public $darah = 50;
  public $jumlahKaki;
  public $keahlian;

  public function __construct($nama, $jumlahKaki, $keahlian) {
    $this->nama = $nama;
    $this->jumlahKaki = $jumlahKaki;
    $this->keahlian = $keahlian;
  }

  abstract public function atraksi();
}

 ?>
